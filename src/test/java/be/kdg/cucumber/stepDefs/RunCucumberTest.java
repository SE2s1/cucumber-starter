package be.kdg.cucumber.stepDefs;


import org.junit.platform.suite.api.*;


/**
 * @author Jan de Rijke.
 */
@Suite
@IncludeEngines("cucumber")
@SelectClasspathResource("features")
// use this if your cucumber test classes are not in the same package as the CucumberRunner class
// supports a comma separated list of values
// can also be specified in junit-platform.properties
// @ConfigurationParameter(key = GLUE_PROPERTY_NAME, value = "be")
public class RunCucumberTest {
}


